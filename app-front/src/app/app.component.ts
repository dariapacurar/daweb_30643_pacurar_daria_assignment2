import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  copyright: string = "© 2020 Copyright";
  myname: string = "Pacurar Daria";

  isEnglish = false;

  constructor(private service: TranslateService) {}

  onChangeLanguage() {
    if (this.isEnglish) {
      this.service.use('ro');
    } else {
      this.service.use('en');
    }

    this.isEnglish = !this.isEnglish;
  }

  ngOnInit(): void {
  }


}
