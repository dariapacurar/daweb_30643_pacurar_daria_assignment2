import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import {TranslateService} from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  personalName: string = "Pacurar Daria";
  personalEmail: string = "pacudari24@gmail.com";
  personalAddress: string = "Cluj Napoca, jud. Cluj, Romania";

  constructor(private _snackBar: MatSnackBar, private translate: TranslateService, private httpClient: HttpClient) { }

  ngOnInit() {
  }

  sendMail (mail: string, text: string) {
    this.httpClient.post<string>('http://127.0.0.1:5000/message', {"email": mail,"message": text})
    .toPromise().then(() => alert('Successfully sent mail')).catch(() => alert('Failed to send mail'))
  }

}