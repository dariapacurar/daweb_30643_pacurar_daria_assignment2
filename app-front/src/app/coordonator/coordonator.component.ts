import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coordonator',
  templateUrl: './coordonator.component.html',
  styleUrls: ['./coordonator.component.scss']
})
export class CoordonatorComponent implements OnInit {
  coordonatorName: string = "Brehar Raluca";
  coordonatorContact: string = "raluca [dot] brehar [at] cs[dot] utcluj[dot] ro";
  constructor() { }

  ngOnInit() {
  }

}
