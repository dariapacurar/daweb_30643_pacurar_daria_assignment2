import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Parser } from 'xml2js';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  articles: string[] = ["https://arxiv.org/pdf/1804.02767.pdf",
"https://github.com/vmtram/recognize-license-plate",
"https://www.edureka.co/blog/tensorflow-object-detection-tutorial/",
"https://developer.android.com/training/basics/firstapp"];

  xmlResponse: any;
  constructor(private httpClient: HttpClient) { 
    this.loadXML();
  }

  ngOnInit() {
  }

  xmlParser(data){
    return new Promise(resolve =>{
      var k: string | number,
      arr = [],
      parser = new Parser({ trim: true, explicitArray: true });
      parser.parseString(data, function(err, result){
        var obj = result.News;
        for(k in obj.article){
          var item = obj.article[k];
          arr.push({
            title: item.title[0],
            link: item.link[0]
          });
        }
        resolve(arr);
      });
    });
  }

  loadXML(){
    this.httpClient.get('/assets/news.xml',
    {
      headers : new HttpHeaders()
      .set('Content-Type', 'text/xml')
      .append('Access-Control-Allow-Methods', 'GET')
      .append('Access-Control-Allow-Origin', '*')
      .append('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method"),
      responseType: 'text'
    })
    .subscribe((data) => {
      this.xmlParser(data)
       .then((data) => {
         this.xmlResponse = data;
        });
    });
}



}
