Pentru a porni serverul(app-back), trebuie efectuata urmatoarea comanda:
	- FLASK_APP=app.py flask run

Pentru a porni clientul (app-front), trebuie efectuata comanda ng serve.

Pentru a trimite un mail pe o adresa proprie, in pagina de contact se va trece email-ul propriu in campul destinat.